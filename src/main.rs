use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::{stdout, BufReader, BufWriter, Error, ErrorKind, SeekFrom};
use std::path::Path;
use std::time::Instant;

use std::os::unix::fs::{FileExt, MetadataExt, OpenOptionsExt}; // yes, "portability issues"

extern crate clap;
use clap::clap_app;

// we will not allocate more than 256 Mb buffer for a single read operation
//
const BUFFER_MB: u64 = 256;

// --- library ----------------------------------------------------------------

// abort without using panic!()
//
fn dont_panic(msg: &str) -> Error {
    eprintln!("{}", msg);
    std::process::exit(1);
}

// "human" formatting for kilobytes and megabytes; we will use the base-10 kb/Mb/Gb instead of
// base-2 kib/Mib/Gib because people don't like to think their disks just shrinked
//
fn humanize(val: u64) -> String {
    if val <= 4096 {
        format!("{}", val)
    } else if val < 1_000_000 {
        format!("{} kb", val / 1000)
    } else if val < 20 * 1_000_000_000 {
        let mb = val / (1_000_000);
        if val == mb * 1_000_000 {
            format!("{} Mb", mb)
        } else {
            let rem = (val - (mb * 1_000_000)) / 100_000;
            format!("{}.{} Mb", mb, rem)
        }
    } else {
        let gb = val / 1_000_000_000;
        if val == gb * 1_000_000_000 {
            format!("{} Gb", gb)
        } else {
            let rem = (val - (gb * 1_000_000_000)) / 100_000_000;
            format!("{}.{} Gb", gb, rem)
        }
    }
}

// abort if unparseable or out of range
//
fn validate(val: &str, min: u64, max: u64) -> Result<u64, Error> {
    match val.parse::<u64>() {
        Ok(n) => {
            if n < min || n > max {
                Err(dont_panic(&format!("value out of range: {:?}", val)))
            } else {
                Ok(n)
            }
        }
        Err(_) => Err(dont_panic(&format!(
            "not a non-negative integer: {:?}",
            val
        ))),
    }
}

// fetch numbers into vector, abort if anything wrong
//
fn fetch_blocks_list<R: Read>(file: R) -> Result<Vec<u64>, Error> {
    BufReader::new(file)
        .lines()
        .map(|line| {
            line.and_then(|v| {
                v.parse()
                    .map_err(|_| dont_panic("parse error on input file"))
            })
        })
        .collect()
}

// output the final version of the bad blocks list
//
fn output_bad_list(bad_blocks: &mut Vec<u64>, output_file: Box<dyn Write>) {
    bad_blocks.sort_unstable();
    bad_blocks.dedup();

    let mut file = BufWriter::new(output_file);
    for bad in bad_blocks.iter() {
        if let Err(e) = file.write(format!("{}\n", bad).as_bytes()) {
            dont_panic(&format!("cannot write to output file: {}", e));
        }
    }
}

// check if src contains an element in the specified range
//
fn is_present(src: &[u64], from: u64, to: u64) -> bool {
    for elem in src.iter() {
        if *elem >= from && *elem < to {
            return true;
        }
    }
    false
}

// --- command-line arguments and configuration validation --------------------
//
struct Args {
    show: bool,
    verbose: bool,
    buffered: bool,
    non_destructive: bool,
    start_time: Instant,
    bytes_per_block: u64,
    blocks_per_area: u64,
    total_bytes_to_scan: u64,
    delay_percent: u64,
    max_bad_areas: u64,
    out_file: Box<dyn Write>,
    repeat_passes: u64,
    first_block: u64,
    last_block: u64,
    area_limit: u64,
    device_name: String,
}

impl Args {
    // this will return a vector and a struct; will actually abort instead of returning Error
    //
    fn parse() -> Result<(Vec<u64>, Args), Error> {
        let arg = clap_app!( manybadblocks =>
        (version: "0.1")
        (author: "Alfonso Martone <alfonso.martone@gmail.com>")
        (about: "Search a device for bad blocks. \
                 Whenever a bad block is found, mark as bad the entire area containing it. \
                 Almost a drop-in replacement for e2fsprogs badblocks tool.")
        (@arg blocksize: -b --("block-size") [bytes]
              "Block size in bytes, default: the device block size" )
        (@arg areasize: -c --("number-of-blocks") [n]
              "Number of blocks in a single good/bad area, default: 4096" )
        (@arg delay: -d --("read-delay-factor") [percent]
              "Delay between reads when no errors encountered, default: 0, max: 500" )
        (@arg maxbad: -e --("max-bad-areas") [n]
              "Maximum number of bad areas (not blocks) before aborting the test, default: 0" )
        (@arg input: -i --("input-file") [FILE]
              "List of known existing bad blocks; default: no list" )
        (@arg nondestructive: -n --("non-destructive-write-mode")
              "Attempt a non-destructive rewrite of blocks (slow; not recommended on SSDs and memory cards)")
        (@arg output: -o --("output-file") [FILE]
              "Where to write the updated bad blocks list; default: stdout" )
        (@arg passes: -p --("num-passes") [n]
              "Repeat scanning until no new blocks discovered in n consecutive scans of the disk; default: 0; max: 10" )
        (@arg show: -s --("show-progress")
              "Write to stdout rough percentage completion of the current pass" )
        (@arg pattern: -t --("test-pattern") [n]
              "Not supported - will be ignored" )
        (@arg verbose: -v --verbose
              "Keep writing the error count on stderr" )
        (@arg destructive: -w --("destructive-write-mode")
              "Not supported - will fallback to non-destructive read-only mode" )
        (@arg buffered: -B --("buffered-mode")
              "Only use buffered I/O (less precise than direct I/O)")
        (@arg bypass: -X --("bypass-exclusive-mode")
              "Ignored; only present for compatibility with original badblocks" )
        (@arg device: *
              "Device/partition name, something like: /dev/sdz1" )
        (@arg lastblock: !required
              "Last block to check, default: last available" )
        (@arg firstblock: !required
              "First block to check, default: 0" )
        ).get_matches();

        let start_time = Instant::now();

        // setup flags; we just ignore unsupported ones
        //
        let buffered = arg.is_present("buffered");
        let show = arg.is_present("show");
        let verbose = arg.is_present("verbose");
        let non_destructive = arg.is_present("nondestructive") || arg.is_present("destructive");

        // how many blocks per area: usually 4096 (original badblocks uses 64);
        // we will call it "area" because we don't actually care if the disk
        // is 512-bytes physical sectors, 4096-bytes logical sectors,
        // 128-kbytes actual I/O read/writes, etc: we want to mark as "bad"
        // an entire "area" containing even a single bad block;
        // the "-b" and "-c" switches will have somewhat the same meaning as
        // in the original badblock
        //
        let blocks_per_area = match arg.value_of("areasize") {
            None => 4096,
            Some(val) => validate(val, 1, 262_144)?,
        };

        // block size can be either 512 or multiples of 1024 up to 1 Mb
        //
        let bytes_per_block = match arg.value_of("blocksize") {
            None => 1024,
            Some(val) => validate(val, 512, 1024 * 1024)?,
        };
        if bytes_per_block != 512 && (bytes_per_block & 0x3ff) != 0 {
            return Err(dont_panic("invalid blocksize value"));
        }
        if bytes_per_block * blocks_per_area > BUFFER_MB * 1024 * 1024 {
            return Err(dont_panic(&format!(
                "blocksize*area exceeds {} Mb",
                BUFFER_MB
            )));
        }

        // delay after successful area check can be up to 500% time used for the previous read
        // (that's somewhat funny in the era without Full Size 3:1 interleaved MFM hard disks)
        //
        let delay_percent = match arg.value_of("delay") {
            None => 0,
            Some(val) => validate(val, 0, 500)?,
        };

        // we're using u64 everywhere - bytes count, block numbers, etc;
        // this should not require changes until you will have to scan
        // disks bigger than 2**63 bytes
        //
        let mut bad_blocks: Vec<u64> = Vec::new();
        if let Some(path) = arg.value_of("input") {
            match File::open(&path) {
                Err(e) => {
                    return Err(dont_panic(&format!(
                        "cannot open bad blocks input file {} ({})",
                        path, e
                    )));
                }
                Ok(file) => match fetch_blocks_list(file) {
                    Ok(list) => bad_blocks = list,
                    Err(e) => {
                        return Err(dont_panic(&format!(
                            "cannot read input file {}: {}",
                            path, e
                        )));
                    }
                },
            }
        }

        let max_bad_areas = match arg.value_of("maxbad") {
            None => 0,
            Some(val) => validate(val, 0, 1 << 32)?,
        };

        let mut out_file = Box::new(stdout()) as Box<dyn Write>;
        if let Some(name) = arg.value_of("output") {
            if name != "-" {
                match File::create(&Path::new(name)) {
                    Ok(file) => {
                        out_file = Box::new(file) as Box<dyn Write>;
                    }
                    Err(e) => {
                        return Err(dont_panic(&format!("cannot create output file: {}", e)));
                    }
                };
            }
        }

        // do not allow an unreasonable number of passes
        //
        let repeat_passes = match arg.value_of("passes") {
            None => 0,
            Some(val) => validate(val, 0, 10)?,
        };

        let mut last_block = match arg.value_of("lastblock") {
            None => 0,
            Some(val) => validate(val, 0, 0xffff_ffff_ffff_ffff / bytes_per_block)?,
        };

        let first_block = match arg.value_of("firstblock") {
            None => 0,
            Some(val) => validate(val, 0, 0xffff_ffff_ffff_ffff / bytes_per_block)?,
        };

        let device_name = arg.value_of("device").unwrap().to_string();

        // warning: Linux-specific stuff incoming:
        // first, we check the S_IFBLK bit (defined in linux/stat.h as octal 0060000) and the
        // device's major/minor numbers; then we scan /sys/class/block/*/dev to extract actual
        // partition (or disk) size and convert it to block size in bytes. mandatory.
        // command-line equivalent is: /sbin/blockdev --getbsz /dev/sdz1
        //
        let meta = match std::fs::metadata(&device_name) {
            Ok(x) => x,
            Err(e) => return Err(dont_panic(&format!("cannot open {}: {}", device_name, e))),
        };

        let mut total_bytes_to_scan: u64 = 0;
        if meta.is_file() {
            // allow regular files to be checked, but warn the user that O_DIRECT on files may be a
            // little flakey under some circumstances
            //
            eprintln!("Warning: {} is reported as a regular file", device_name);
            if !buffered {
                eprintln!("         Direct (non buffered) test may give false positives");
            }
            total_bytes_to_scan = meta.len();
        } else if (meta.mode() & 0o060_000 == 0) || (meta.rdev() == 0) {
            return Err(dont_panic(&format!(
                "{} is not a valid block device",
                device_name
            )));
        } else {
            // having valid major/minor values from the meta rdev field, we can either scan
            // /proc/partitions to get the device size (in 512 bytes units) or /sys/class/block;
            // we'll go for the latter, as it can give us more information (currently not used:
            // ro, start, stat, uevent)
            //
            if let Ok(block_devices) = std::fs::read_dir("/sys/class/block") {
                let dev = format!("{}:{}\n", (meta.rdev() >> 8) & 255, meta.rdev() & 255);
                let _disk = format!("{}:{}\n", (meta.rdev() >> 8) & 255, meta.rdev() & 240);

                for device_dir in block_devices {
                    if let Ok(dir) = device_dir {
                        let mut info = dir.path();
                        info.push("dev");
                        if let Ok(current) = std::fs::read_to_string(&info) {
                            if current == dev {
                                info.pop();
                                info.push("size");
                                if let Ok(sectors) = std::fs::read_to_string(info) {
                                    total_bytes_to_scan =
                                        sectors.trim().parse::<u64>().unwrap() * 512;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if total_bytes_to_scan == 0 {
                return Err(dont_panic("cannot get device size"));
            }
        };

        if total_bytes_to_scan < bytes_per_block * blocks_per_area {
            return Err(dont_panic(&format!(
                "partition size ({}) is smaller than requested area size",
                humanize(total_bytes_to_scan)
            )));
        }

        // adjust last_block and first_block, where needed
        //
        let max_last_block = (total_bytes_to_scan + bytes_per_block - 1) / bytes_per_block;
        if last_block == 0 {
            last_block = max_last_block;
        } else if last_block > max_last_block {
            return Err(dont_panic(&format!(
                "last block out of range; maximum allowed value was {}",
                max_last_block
            )));
        }
        if first_block >= last_block {
            return Err(dont_panic("last block cannot be before first block"));
        }
        let area_limit = (last_block + blocks_per_area - 1) / blocks_per_area;

        if verbose {
            eprintln!(
                "Checking blocks {} to {} ({} areas)",
                first_block,
                last_block - 1,
                area_limit
            );
            eprintln!(
                "    Block size: {}  Area size: {}  Total size: {}",
                humanize(bytes_per_block),
                humanize(bytes_per_block * blocks_per_area),
                humanize(total_bytes_to_scan)
            );

            if !bad_blocks.is_empty() {
                eprintln!("    Total pre-loaded bad sectors: {}", bad_blocks.len());
            }
        }

        bad_blocks.sort_unstable();
        bad_blocks.dedup();

        Ok((
            bad_blocks,
            Args {
                show,
                verbose,
                buffered,
                non_destructive,
                start_time,
                bytes_per_block,
                blocks_per_area,
                total_bytes_to_scan,
                delay_percent,
                max_bad_areas,
                out_file,
                repeat_passes,
                first_block,
                last_block,
                area_limit,
                device_name,
            },
        ))
    }

    fn open(&self) -> Result<File, Error> {
        // Linux O_DIRECT flag, as of GNU_SOURCE, is 0x4000:
        let direct: i32 = if self.buffered { 0 } else { 0x4000 };

        match OpenOptions::new()
            .read(true)
            .write(self.non_destructive)
            .custom_flags(direct)
            .open(&self.device_name)
        {
            Ok(file) => Ok(file),
            Err(e) => Err(dont_panic(&format!(
                "cannot open {}: {}",
                self.device_name, e
            ))),
        }
    }

    fn running_time(&self) -> String {
        let total_time = self.start_time.elapsed().as_secs();
        let minutes = total_time / 60;
        let seconds = total_time % 60;
        format!("{}:{:02}", minutes, seconds)
    }
}

// --- real meat comes here ---------------------------------------------------
//
fn main() {
    let (mut bad_blocks, config) = Args::parse().unwrap();
    let mut last_update = config.start_time;

    // allocate and fill a read buffer
    //
    let full_chunk_size = config.bytes_per_block * config.blocks_per_area;
    let mut buffer = vec![0_u8; full_chunk_size as usize];
    let buffer = buffer.as_mut_slice();

    // bad blocks list to be merged with the (usually empty) one given by the user, and counter for
    // new bad blocks actually found while scanning (not including known ones and area fills):
    //
    let mut new_blocks_found = Vec::new();
    let mut total_bad_found = 0;

    // counter for "multiple passes without errors" feature, if needed:
    //
    let mut current_pass = config.repeat_passes;

    // in case of bad blocks, we will have to close and reopen the device
    //
    let mut device = config.open().unwrap();

    // repeat the possible multiple passes until we're done:
    //
    loop {
        let mut any_new_blocks = false; // flag: were *actually* found new bad blocks?
        let mut current_area = config.first_block / config.blocks_per_area;

        if config.verbose {
            eprint!(
                "Checking for bad blocks ({}):",
                if config.non_destructive {
                    "non-destructive write mode"
                } else {
                    "read-only test"
                }
            );

            if config.show {
                eprintln!();
            }
        }

        // before starting the current pass, flush out duplicates
        //
        new_blocks_found.sort_unstable();
        new_blocks_found.dedup();

        // do a full scan, area by area:
        //
        loop {
            if current_area >= config.area_limit {
                break;
            }

            // we're not going to test areas known to have bad blocks
            //
            let from = current_area * config.blocks_per_area;
            let to = (current_area + 1) * config.blocks_per_area;
            if is_present(&bad_blocks, from, to) {
                // something bad is in our area: nuke it
                //
                for surely_bad in from..to {
                    new_blocks_found.push(surely_bad);
                }

                // just skip the nuked area
                //
                current_area += 1;
                continue;
            }

            // scan a full area
            //
            let mut bytes_to_check = full_chunk_size as usize;
            let mut position = current_area * config.blocks_per_area * config.bytes_per_block;
            if position + bytes_to_check as u64 > config.total_bytes_to_scan {
                bytes_to_check = (config.total_bytes_to_scan - position) as usize;
            }

            let chunk_start_time = Instant::now();
            loop {
                // ready to scan: device seek shall not fail, or else...
                //
                if let Err(e) = device.seek(SeekFrom::Start(position)) {
                    dont_panic(&format!("cannot seek device: {}", e));
                }

                // try to read
                //
                let mut result = device.read_at(&mut buffer[..bytes_to_check], position);
                let mut bytes_done: usize = 0;
                if let Ok(n) = result {
                    bytes_done = n;

                    if n > 0 && config.non_destructive {
                        result = device.write_at(&buffer[..bytes_to_check], position);

                        // any result different than "exactly n bytes were successfully written"
                        // means we have to nuke the entire area:
                        //
                        if let Ok(written) = result {
                            if written != n {
                                result = Err(Error::new(
                                    ErrorKind::Other,
                                    &format!("rewrite failed ({}/{})", written, n)[..],
                                ));
                            }
                        }
                    }
                }

                // if we got a read error or a write error:
                //
                if let Err(e) = result {
                    match e.kind() {
                        ErrorKind::Interrupted => {
                            dont_panic(&format!("interrupted at block {}", from));
                        }
                        ErrorKind::InvalidInput => {
                            dont_panic("invalid input error (try buffered mode; was device in sleep mode?)");
                        }
                        ErrorKind::Other => {
                            // actual read error spotted, going on
                        }
                        shouldnt => {
                            dont_panic(&format!("ERROR: {:?}", shouldnt));
                        }
                    }

                    // there's at least a bad block in this area:
                    // nuke it and remember that this scan wasn't without errors:
                    //
                    for surely_bad in from..to {
                        new_blocks_found.push(surely_bad);
                    }

                    any_new_blocks = true;
                    total_bad_found += 1;
                    if config.max_bad_areas > 0 && total_bad_found >= config.max_bad_areas {
                        dont_panic(&format!(
                            "too many new bad areas found ({}), aborting...",
                            config.max_bad_areas
                        ));
                    }

                    // we're here because of some I/O error: close and hope the kernel forgives
                    // and forgets, and open again for subsequent reads:
                    //
                    device = config.open().unwrap();

                    // don't bother testing the remaining sub-chunks: this area has been nuked
                    //
                    break;
                }

                // this sub-chunk was without errors, stop if no more sub-chunks:
                //
                position += bytes_done as u64;
                bytes_to_check -= bytes_done;
                if bytes_to_check == 0 || position == config.total_bytes_to_scan {
                    break;
                }

                // if post-scan delay is required, pause for the specified percentage:
                //
                if config.delay_percent > 0 {
                    let delay = chunk_start_time
                        .elapsed()
                        .checked_mul(config.delay_percent as u32)
                        .unwrap()
                        .checked_div(100)
                        .unwrap();
                    std::thread::sleep(delay);
                }
            }

            if config.show {
                let elapsed = last_update.elapsed();
                if elapsed.as_secs() > 0 || elapsed.subsec_millis() >= 500 {
                    let done = (current_area + 1) * config.blocks_per_area * 10000
                        / (config.last_block + 1);
                    let percent = done / 100;
                    let fraction = done % 100;

                    eprint!(
                        "\r    {}.{:02}%, {} elapsed, {} found  ",
                        percent,
                        fraction,
                        config.running_time(),
                        total_bad_found
                    );
                    last_update = Instant::now();
                }
            }
            current_area += 1;
        }

        // check if we're done: either "no multiple passes", or "all multiple passes were ok":
        //
        if config.repeat_passes == 0 {
            break;
        }
        if any_new_blocks {
            // new blocks were found, move them in the default bad list, restart all passes:
            //
            bad_blocks.extend(&new_blocks_found);
            current_pass = config.repeat_passes;
        } else {
            current_pass -= 1;
            if current_pass == 0 {
                break;
            }
        }

        if config.show {
            eprintln!(
                "\r    Pass completed, {} bad block(s) found.",
                total_bad_found
            );
        }
    }

    // flush duplicates to print the correct count:
    //
    new_blocks_found.sort_unstable();
    new_blocks_found.dedup();

    eprintln!(
        "\r    completed in {}, with {}+{} bad blocks total.",
        config.running_time(),
        bad_blocks.len(),
        new_blocks_found.len(),
    );

    bad_blocks.extend(&new_blocks_found);
    output_bad_list(&mut bad_blocks, config.out_file);
}
