## What?

Searches a device (disk or partition) for bad blocks:

* may be used as a drop-in replacement for the original *badblocks* utility from *e2fsprogs* package (featuring a superset of *badblocks* syntax), when formatting disks.
* will mark as bad the entire "area" containing a bad block (while *badblocks* only marked that single bad block).
* can perform either read-only test (safe for mounted partitions) or non-destructive write-after-read test.

## Compile and install

You need [Rust](https://rustup.rs). Compile with the usual *cargo build --release* and install with something like:

    cp target/release/manybadblocks /sbin

## Why?

I was once gifted a "failing" 320 Gb hard disk. Too many damaged sectors. Apparently it had some areas where bad sectors (or going-to-fail sectors) were most frequent. I manually restarted *badblocks* every time it blocked on some bad sector and annotated the largest areas cleared by *badblocks* without errors. I was able to get 190 Gb total in four non-failing partitions and happily used it for more than three years without problems (anyways always used for non-critical data like movies and music storage).

Some of my memory cards started showing some new bad block. As usual, this happens during holidays. I didn't want to wait to buy new ones, as long as I could get a few reasonably safe gigabytes for my embedded boards. The original *badblocks* kept running for ages, I didn't want to waste my time checking and stopping and possibly rebooting the machine where they were checked. I needed to either patch the *badblocks* mess or to rewrite it.

* *Use Case:* memory cards and old disks showing "some" bad blocks but not blatantly failing otherwise.

## How?

The *badblocks* source dates back to 1993. In those epic years, bad blocks were sparse, isolated ones, and the "make filesystem" utilities (*mkfs.ext4* and the likes) marked the *badblocks* scanned list as "never use".

It's been quite a while disks/memorycards firmwares autonomously remap bad blocks as soon as they are going to fail. A disk or memorycard actually showing new bad blocks is probably going to fail soon. Yet sometimes it happens that you feel you don't have enough reasons to dispose of it. Notwithstanding some *smartctl* and *hdparm* warnings, a presumably "failed" disk may still safely work for months or years giving you some extra frivolous disk space.

Scanning for new bad sectors is as simple as physically reading them (this also makes the disk/memorycard firmware try to remap if needed): if it fails, the sectors have to be marked as "never use" by those make-filesystem programs.

Traditional spinning disks may enjoy a non-destructive write test (correctly read sectors get re-written in place).

More complex options are only useful for traditional spinning disks (non-destructive test: rewriting the sector contents, pattern/destructive checking: more writes on the same sector before restoring its contents).
